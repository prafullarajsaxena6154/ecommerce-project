document.addEventListener("DOMContentLoaded", function () {
    let dataform = document.getElementById('dataForm');
    dataform.addEventListener('submit', handleForm);
    document.getElementById('checkErr').style.display = 'none';
    document.getElementById('passCheckErr').style.display = 'none';
    document.getElementById('passwordErr').style.display = 'none';
    document.getElementById('emailErr').style.display = 'none';
    document.getElementById('lNameErr').style.display = 'none';
    document.getElementById('fNameErr').style.display = 'none';
});
let check = '';
let firstName = '';
let lastName = '';
let email = '';
let password = '';
let passCheck = '';



function handleForm(event) {
    event.preventDefault();

    firstName = document.getElementById('fName').value;
    firstName = firstName.trim();

    lastName = document.getElementById('lName').value;
    lastName = lastName.trim();

    email = document.getElementById('email').value;
    email = email.trim();

    password = document.getElementById('password').value;

    passCheck = document.getElementById('passCheck').value;

    check = document.getElementById('check').checked;

    if (password.includes(' ') || password.length == 0 || !check || password !== passCheck || !(/^[a-zA-Z]+$/).test(firstName) || !(/^[a-zA-Z]+$/).test(lastName) || !email.includes('@')) {
        document.querySelector('#loader').style.display = 'none';
        if (!(/^[a-zA-Z]+$/).test(firstName)) {
            document.getElementById('fNameErr').style.display = 'flex';
        }
        else if ((/^[a-zA-Z]+$/).test(firstName)) {
            document.getElementById('fNameErr').style.display = 'none';
        }

        if (!(/^[a-zA-Z]+$/).test(lastName)) {
            document.getElementById('lNameErr').style.display = 'flex';
        }
        else if ((/^[a-zA-Z]+$/).test(lastName)) {
            document.getElementById('lNameErr').style.display = 'none';
        }

        if ((!email.includes('@')) || email.includes(' ')) {
            document.getElementById('emailErr').style.display = 'flex';
        }
        else if ((email.includes('@')) || (!email.includes(' '))) {
            document.getElementById('emailErr').style.display = 'none';
        }

        if (password.includes(' ') || password.length == 0) {
            document.getElementById('passwordErr').style.display = 'flex';
        }
        else if ((!password.includes(' ')) || (!password == 'null')) {
            document.getElementById('passwordErr').style.display = 'none';
        }

        if (password !== passCheck) {
            document.getElementById('passCheckErr').style.display = 'flex';
        }
        else if (password === passCheck) {
            document.getElementById('passCheckErr').style.display = 'none';
        }

        if (!check) {
            document.getElementById('checkErr').style.display = 'flex';
        }
        else if (check) {
            document.getElementById('checkErr').style.display = 'none';
        }
    }
    else {
        document.querySelector('#loader').style.display = 'block';
        document.getElementById('checkErr').style.display = 'none';
        document.getElementById('passCheckErr').style.display = 'none';
        document.getElementById('passwordErr').style.display = 'none';
        document.getElementById('emailErr').style.display = 'none';
        document.getElementById('lNameErr').style.display = 'none';
        document.getElementById('fNameErr').style.display = 'none';
        window.location.href = './products.html';
    }
}
// document.querySelector('#loader').style.display = 'block';

let dataArray = new Array();
fetch('https://fakestoreapi.com/products')
    .then(res => res.json())
    .then(json => {
        document.querySelector('#loader').style.display = 'none';
        const container = document.getElementById("container");
        document.getElementById('nav-bar').style.display='flex';
        if (json.length === 0) {
            container.innerHTML = `<h1>No products found</h1>`;
        }
        else {
            dataArray = json;
            container.innerHTML = '';
            let headingArray = [];
            dataArray.forEach((ele) => {
                if (!headingArray.includes(ele.category)) {
                    headingArray.push(ele.category);
                }
                container.innerHTML += `<div class="productCard" id="${ele.category}">
                        <div id="imgContainer"><div><img id="image" src=${ele.image}></div></div>
                        <div class="details">
                           <!-- <div id="id">ID:${ele.id}</div> -->
                            <div id="title"><h4>${ele.title}</h4></div>
                            <div id="price"><h4>&dollar;${ele.price}</h4></div>
                            <!-- <div id="category">Category:${ele.category}</div> -->
                            <div id="description"><h5>About: ${ele.description}</h5></div>
                            <div id="rating">
                                <div id="rate"><h5>Rating:${ele.rating.rate}/5</h5></div>
                                <div id="ratedBy"><h5>(${ele.rating.count})</h5></div>
                            </div>
                        </div>
                    </div>`;
            });
            headingArray.forEach((ele) => {
                container.innerHTML += `<div id="${ele.toUpperCase()}"><div id="category-heading"><h1>${ele.toUpperCase()}</h1></div></div>`
            })
        }
    }).catch((err) => {
        const container = document.getElementById("container");
        container.innerHTML = `<div class="fetchErr">
                                <img id="error404" src="https://cdn.dribbble.com/users/1537480/screenshots/7199901/media/04f1bc09a3a16f5efc155fe9ea829cbc.png?compress=1&resize=400x300&vertical=center"></img>
                                <button id="submitB">RETRY</button>
                               </div>`;
        console.error(err);
        document.addEventListener("click", function () {
            window.location.href = './index.html';
        })
    });